
/* XXX: seems, that talking with ram is done on diffrent address o_O */
/* i2cdump 5 0x1b w 0x0000 0x64 */
static void asusec_dockram_init(struct i2c_client *client){
	global_ec->dockram_client.adapter  = client->adapter;
	global_ec->dockram_client.addr	   = 0x1b; /* XXX: hardcoded address */
	global_ec->dockram_client.detected = client->detected; 
	global_ec->dockram_client.dev	   = client->dev;
	global_ec->dockram_client.flags	   = client->flags;
	global_ec->dockram_client.irq	   = client->irq;
	strcpy(global_ec->dockram_client.name, client->name);

        /* test if dockram is available */
	ret = i2c_smbus_write_word_data(&global_ec->dockram_client, 0x64, 0x0000);
	XXX("i2c-test=%d", ret);

	/* reset dock - do we need any of these (?) */
	gpio_set_value(TEGRA_GPIO_PS3, 0);
	msleep(50);
	gpio_set_value(TEGRA_GPIO_PS3, 1);

	for(i = 0x1; i <= 0x4; i++) {
		asusec_dockram_read_data(i, buf);
		XXX("Read: [%d], %s", buf[0], &buf[1]);
	}
}

/* Reads data from dock ram - first byte is string length counting \0, */
/* 	therefore message "xyz" has 5 bytes - [0x4,'x','y','z','\0'] */
static int asusec_dockram_read_data(int cmd, char *buf) {
	int ret=0;
	ret = i2c_smbus_read_i2c_block_data(&global_ec->dockram_client,
					    cmd, 32, buf);
	if (ret < 0)
		XXX("Fail to read dockram data, status %d", ret);
	return ret;
}
