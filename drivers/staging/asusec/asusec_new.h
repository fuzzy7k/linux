#ifndef _ASUSEC_H
#define _ASUSEC_H

#include <linux/delay.h>
#include <linux/workqueue.h>
#include <linux/completion.h>
#include <linux/i2c.h>
#include <xxx.h>

#define TEGRA_GPIO_PS2			146 // AP_WAKE
#define TEGRA_GPIO_PS3			147 // EC_REQUEST
#define TEGRA_GPIO_PX5			189 // DOCK_IN	LOW: dock-in, HIGH: dock disconnected
#define TEGRA_GPIO_PS4			148 // HALL_SENSOR, SW_LID

#define ASUSEC_OBF_MASK			0x1
#define ASUSEC_KEY_MASK			(ASUSEC_OBF_MASK + 0x4)
#define ASUSEC_KBC_MASK			(ASUSEC_OBF_MASK + 0x8)
#define ASUSEC_AUX_MASK			(ASUSEC_OBF_MASK + 0x20)
#define ASUSEC_SCI_MASK			(ASUSEC_OBF_MASK + 0x40)
#define ASUSEC_SMI_MASK			(ASUSEC_OBF_MASK + 0x80)

#define ASUSEC_SMI_HANDSHAKING		0x50
#define ASUSEC_SMI_WAKE			0x53
#define ASUSEC_SMI_RESET		0x5F

struct asusec_chip {
	/* char ec_model_name[32]; */
	/* char ec_fw_version[32]; */
	/* char ec_coding_fmt[32]; */
	/* char ec_pid_version[32]; */
        struct i2c_client *client;
        struct i2c_client dockram_client;
        struct workqueue_struct *dock_queue;
	struct completion write;

	u8 ec_data[32];
};

#endif /* _ASUSEC_H */
