#include <linux/delay.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/err.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/gpio.h>
#include <linux/input.h>
#include <linux/interrupt.h>
#include <linux/workqueue.h>
#include <linux/completion.h>
#include <linux/jiffies.h>
#include <linux/timer.h>

#include "asusec_new.h"
#include "asusec_keypad.h"

struct asusec_chip *ec;
static struct input_dev *keypad_dev;

static void key_process(const u8 *buf)
{
	int keycode = 0;
	int value = 0;

	if ( buf[2] == 0xe0 ) {
		if (buf[0] == 6) {
			value = (buf[4] == 0x12 || buf[4] == 0x59); //shift + note2 keys
			keycode = (value ?
				   keypad_mapping[buf[6]] :
				   keypad_mapping[buf[4]]);
		}
		else {
			value = (buf[3] != 0xf0);
			keycode = (value ?
				   keypad_mapping[buf[3]] :
				   keypad_mapping[buf[4]]);
		}
	}
	else {
		value = (buf[2] != 0xf0);
		keycode = (value ?
			   keypad_mapping[buf[2]] :
			   keypad_mapping[buf[3]]);
	}

	if (keycode) {
		input_report_key(keypad_dev, keycode, value);
		input_sync(keypad_dev);
	} else {
		dev_warn(&ec->client->dev,"Undefined key code: code = 0x%x, value = 0x%x, extra = 0x%x",buf[2],buf[3],buf[4]);
	}
}

static void sci_process(const u8 *buf)
{
	int keycode = asusec_kp_sci_table[buf[2]];

	if (keycode) {
		input_report_key(keypad_dev, keycode, 1);
		input_sync(keypad_dev);
		input_report_key(keypad_dev, keycode, 0);
		input_sync(keypad_dev);
	} else if (buf[2]) {	//filter out code 0, happens every 8th sci
		dev_warn(&ec->client->dev,"Undefined sci code: code = 0x%x",buf[2]);
	}
}

int asusec_i2c_read_data(u8 *buf)
{
	int ret = 0;

	ret = i2c_smbus_read_i2c_block_data(ec->client, 0x6A, 8, buf);
	if (ret < 0) {
		dev_err(&ec->client->dev,"Fail to read data, status %d", ret);
	}
	return ret;
}

int asusec_i2c_write_data(u16 data)
{
	int ret = 0;

	dev_dbg(&ec->client->dev," > write %x", data);
	ret = i2c_smbus_write_word_data(ec->client, 0x64, data);

	if (ret < 0) {
		dev_err(&ec->client->dev,"Fail to write data, status %d", ret);
		return ret;
	}

	ret = wait_for_completion_interruptible_timeout(&ec->write, HZ/1);

	if (ret < 0) {
		dev_err(&ec->client->dev,"need to Kill write queue, %d", ret);
	}
	else if ( ret == 0 ) {
		dev_err(&ec->client->dev," < No command completed ACK!");
		return EREMOTEIO;
	}
	else {
		dev_dbg(&ec->client->dev," - jiffies till timeout = %d", ret);
	}

	return 0;
}

/* Work queue functions */
static void asusec_smi_reset(struct work_struct *w)
{
	dev_warn(&ec->client->dev,"Reset Dock");
	gpio_set_value(TEGRA_GPIO_PS3, 0);
	msleep(50);
	gpio_set_value(TEGRA_GPIO_PS3, 1);
}
DECLARE_WORK(dock_reset, asusec_smi_reset);

static void asusec_keypad_led_on(struct work_struct *w)
{
	dev_dbg(&ec->client->dev,"send led on cmd seq");
	asusec_i2c_write_data(0xED00);
	asusec_i2c_write_data(0x0400);
}
DECLARE_WORK(led_on, asusec_keypad_led_on);

static void asusec_keypad_led_off(struct work_struct *w)
{
	dev_dbg(&ec->client->dev,"send led off cmd seq");
	asusec_i2c_write_data(0xED00);
	asusec_i2c_write_data(0x0000);
}
DECLARE_WORK(led_off, asusec_keypad_led_off);

static void asusec_keypad_enable(struct work_struct *w)
{
	dev_dbg(&ec->client->dev,"send keypad enable cmd");
	if (asusec_i2c_write_data(0xF400) < 0)
		dev_err(&ec->client->dev,"Send keypad enable cmd failed");
}
DECLARE_WORK(keypad_enable, asusec_keypad_enable);

static void asusec_keypad_disable(struct work_struct *w)
{
	dev_dbg(&ec->client->dev,"Send keypad disable cmd");
	if (asusec_i2c_write_data(0xF500) < 0)
		dev_err(&ec->client->dev,"Send keypad disable cmd failed");
}
DECLARE_WORK(keypad_disable, asusec_keypad_disable);

static irqreturn_t asusec_sw_lid_interrupt(int irq, void *dev_id);
static void asusec_dock_process(struct work_struct *w)
{
	int ret;

	ret = asusec_i2c_read_data(ec->ec_data);

	if (ret < 0) {
		del_timer(&keypad_dev->timer);
		queue_work(ec->dock_queue, &dock_reset);
		return;
	};

	switch (ec->ec_data[1]) {
		case ASUSEC_AUX_MASK:
			break;
		case ASUSEC_KEY_MASK:
			key_process(ec->ec_data);
			break;
		case ASUSEC_SCI_MASK:
			sci_process(ec->ec_data);
			break;
		case ASUSEC_KBC_MASK:
			if (ec->ec_data[2] == ASUSEC_PS2_ACK) {
				dev_dbg(&ec->client->dev," < command completed successfully");
				complete(&ec->write);
			} else {
				dev_warn(&ec->client->dev,"ASUSEC_KBC_MASK = 0x%x, code = 0x%x, value = 0x%x, extra = 0x%x",ec->ec_data[1],ec->ec_data[2],ec->ec_data[3],ec->ec_data[4]);
			}
			break;
		case ASUSEC_SMI_MASK:
			if (ec->ec_data[2] == ASUSEC_SMI_HANDSHAKING){
				dev_warn(&ec->client->dev,"ASUSEC_SMI_HANDSHAKING");
				asusec_sw_lid_interrupt(0, &ec->client);
			} else if (ec->ec_data[2] == ASUSEC_SMI_RESET){
				dev_warn(&ec->client->dev,"ASUSEC_SMI_RESET");
				queue_work(ec->dock_queue, &dock_reset);
			} else if (ec->ec_data[2] == ASUSEC_SMI_WAKE){
				dev_warn(&ec->client->dev,"ASUSEC_SMI_WAKE");
			} else  {
				dev_warn(&ec->client->dev,"Unknown SMI code, 0x%x, 0x%x, 0x%x",ec->ec_data[2],ec->ec_data[3],ec->ec_data[4]);
			}
			break;
		default:
			dev_warn(&ec->client->dev,"Unrecognized ec->ec_datafer; type = 0x%x, code = 0x%x, value = 0x%x, extra = 0x%x",ec->ec_data[1],ec->ec_data[2],ec->ec_data[3],ec->ec_data[4]);

	}
	return;
}
DECLARE_WORK(dock_process, asusec_dock_process);

/* declare interrupts */
static irqreturn_t asusec_dock_status_interrupt(int irq, void *dev_id)
{
	if (gpio_get_value(TEGRA_GPIO_PX5)) {
		dev_warn(&ec->client->dev,"Dock is disconnected");
		ec->write.done = 0;
	} else {
		dev_warn(&ec->client->dev,"Dock is attached!");
		queue_work(ec->dock_queue, &dock_reset);
	}

	return IRQ_HANDLED;
}

static irqreturn_t asusec_sw_lid_interrupt(int irq, void *dev_id) 
{
	if (gpio_get_value(TEGRA_GPIO_PS4)) {
		dev_info(&ec->client->dev,"Lid is open");
		queue_work(ec->dock_queue, &keypad_enable);
	} else {
		dev_info(&ec->client->dev,"Lid is closed");
		queue_work(ec->dock_queue, &keypad_disable);
	}

	return IRQ_HANDLED;
}

static irqreturn_t asusec_ap_wake_interrupt(int irq, void *dev_id)
{
	dev_vdbg(&ec->client->dev,"IRQ Recieved");
	queue_work(system_highpri_wq, &dock_process);

	return IRQ_HANDLED;
}

static int init_interrupts(struct i2c_client *client) {
	int result_code;
	unsigned dock_status_irq, sw_lid_irq, ap_wake_irq;

	dev_dbg(&ec->client->dev,"preparing dock irq handler");
	dock_status_irq = gpio_to_irq(TEGRA_GPIO_PX5);
	result_code = request_irq(dock_status_irq,
				  asusec_dock_status_interrupt,
				  IRQF_SHARED |
				  IRQF_TRIGGER_RISING |
				  IRQF_TRIGGER_FALLING,
				  "asusec_dock_status",
				  client);
	if(result_code < 0) {
		dev_err(&ec->client->dev,"Couldn't register dock in (%d -> %d)",
		    dock_status_irq, result_code);
		return result_code;
	}

	dev_dbg(&ec->client->dev,"preparing sw_lid irq handler");
	sw_lid_irq = gpio_to_irq(TEGRA_GPIO_PS4);
	result_code = request_irq(sw_lid_irq,
				  asusec_sw_lid_interrupt,
				  IRQF_SHARED |
				  IRQF_TRIGGER_RISING |
				  IRQF_TRIGGER_FALLING,
				  "asusec_sw_lid",
				  client);
	if(result_code < 0) {
		dev_err(&ec->client->dev,"Couldn't register sw lid in (%d -> %d)",
		    sw_lid_irq, result_code);
		return result_code;
	}

	dev_dbg(&ec->client->dev,"preparing ap_wake irq handler");
	ap_wake_irq = gpio_to_irq(TEGRA_GPIO_PS2);
	result_code = request_irq(ap_wake_irq,
				  asusec_ap_wake_interrupt,
				  IRQF_SHARED |
				  IRQF_TRIGGER_FALLING,
				  "asusec_ap_wake",
				  client);
	if(result_code < 0) {
		dev_err(&ec->client->dev,"Couldn't register ap wake in (%d -> %d)",
		    ap_wake_irq, result_code);
		return result_code;
	}

	result_code = gpio_direction_output(TEGRA_GPIO_PS3,1);
	if (result_code < 0) {
		dev_err(&ec->client->dev,"gpio_direction_input failed for input %d\n", TEGRA_GPIO_PS3);
	}

	return 0;
}	

static int system_event_handler(struct input_dev *keypad_dev, unsigned int type, unsigned int code, int value){
//	if ((ec_chip->op_mode == 0) && (ec_chip->dock_in)){
		if ((type == EV_LED) && (code == LED_CAPSL)){
			if(value == 0){
				queue_work(ec->dock_queue, &led_off);
				return 0;
			} else {
				queue_work(ec->dock_queue, &led_on);
				return 0;
			}
		}
//	}
	return -ENOTTY;
}

int keypad_init(void)
{
	int error, i;

	keypad_dev = input_allocate_device();
	if (!keypad_dev) {
		printk(KERN_ERR "not enough memory\n");
		error = -ENOMEM;
	}

	keypad_dev->name = "asusec keypad";
	keypad_dev->dev.parent = &ec->client->dev;
	keypad_dev->event = system_event_handler;

	set_bit(EV_KEY, keypad_dev->evbit);
	set_bit(EV_REP, keypad_dev->evbit);

	for (i = 0; i < 246; i++)
		set_bit(i,keypad_dev->keybit);
	input_set_capability(keypad_dev, EV_LED, LED_CAPSL);

	error = input_register_device(keypad_dev);
	if (error) {
		printk(KERN_ERR "failed to register device\n");
		goto err_free_dev;
	}
	dev_dbg(&ec->client->dev,"input device registered");

	return 0;

err_free_dev:
	input_free_device(keypad_dev);
	return error;
}

static int asusec_probe(struct i2c_client *client,
			const struct i2c_device_id *id)
{
	int result_code;

	dev_dbg(&client->dev,"allocating ec_chip private memory");
	ec = kzalloc(sizeof (struct asusec_chip), GFP_KERNEL);
	if (!ec) {
		dev_err(&client->dev,"Memory allocation fails\n");
		return -ENOMEM;
	}
	i2c_set_clientdata(client, ec);

	dev_dbg(&client->dev,"creating workqueues");
	ec->dock_queue = alloc_workqueue("dock queue", 0, 0);
	ec->client = client;

	init_completion(&ec->write);

	keypad_init();

	result_code = init_interrupts(client);
	if(result_code < 0) {
		return result_code;
	}

	msleep(3000);

	/* Initially, check dock status */
	asusec_dock_status_interrupt(0, client);

	return 0;
}

static int asusec_remove(struct i2c_client *client)
{
	unsigned dock_status_irq, sw_lid_irq, ap_wake_irq;
	dev_dbg(&client->dev,"unloading driver");

	dock_status_irq = gpio_to_irq(TEGRA_GPIO_PX5);
	sw_lid_irq	= gpio_to_irq(TEGRA_GPIO_PS4);
	ap_wake_irq     = gpio_to_irq(TEGRA_GPIO_PS2);

	queue_work(ec->dock_queue, &keypad_disable);
	drain_workqueue(ec->dock_queue);

	free_irq (dock_status_irq, client);
	free_irq (sw_lid_irq, client);
	free_irq (ap_wake_irq, client);

	input_unregister_device(keypad_dev);
	dev_dbg(&ec->client->dev,"keypad unregistered");

	destroy_workqueue(ec->dock_queue);

	kfree(ec);

	return 0;
}

static const struct i2c_device_id asusec_id[] = {
	{"asusec", 0},
	{}
};

MODULE_DEVICE_TABLE(i2c, asusec_id);

#ifdef CONFIG_OF
static const struct of_device_id asusec_of_match[] = {
	{ .compatible = "asus,asusec", },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, asusec_of_match);
#endif

static struct i2c_driver asusec_driver = {
	.class	= I2C_CLASS_HWMON,
	.driver	 = {
		.name		= "asusec",
		.owner		= THIS_MODULE,
		.of_match_table = of_match_ptr(asusec_of_match),
	},
	.probe	 = asusec_probe,
	.remove	 = asusec_remove,
	.id_table = asusec_id,
};

module_i2c_driver(asusec_driver);

MODULE_DESCRIPTION("I2C driver for Asus Embedded Chip");
MODULE_AUTHOR("Daniel Kochmański <jackdaniel@hellsgate.pl>");
MODULE_LICENSE("GPL v2");
