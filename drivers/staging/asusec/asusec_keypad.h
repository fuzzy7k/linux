#ifndef _ASUSEC_KEYPAD_H
#define _ASUSEC_KEYPAD_H

#include <linux/input.h>
#define ASUSEC_PS2_ACK			0xFA

static unsigned int keypad_mapping[] =
  { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, KEY_TAB, KEY_GRAVE, 0, 0,
    KEY_RIGHTALT, KEY_LEFTSHIFT, 0, KEY_LEFTCTRL, KEY_Q, KEY_1, 0, 0, 0,
    KEY_Z, KEY_S, KEY_A, KEY_W, KEY_2, KEY_LEFTMETA, 0, KEY_C, KEY_X,
    KEY_D, KEY_E, KEY_4, KEY_3, KEY_LEFTALT, 0, KEY_SPACE, KEY_V, KEY_F,
    KEY_T, KEY_R, KEY_5, KEY_DELETE, 0, KEY_N, KEY_B, KEY_H, KEY_G, KEY_Y,
    KEY_6, 0, 0, 0, KEY_M, KEY_J, KEY_U, KEY_7, KEY_8, KEY_ESC, 0, KEY_COMMA,
    KEY_K, KEY_I, KEY_O, KEY_0, KEY_9, 0, 0, KEY_DOT, KEY_SLASH, KEY_L,
    KEY_SEMICOLON, KEY_P, KEY_MINUS, 0, 0, 0, KEY_APOSTROPHE, 0,
    KEY_LEFTBRACE, KEY_EQUAL, 0, 0, KEY_CAPSLOCK, KEY_RIGHTSHIFT,
    KEY_ENTER, KEY_RIGHTBRACE, 0, KEY_BACKSLASH, 0, 0, 0, 0, 0, 0, 0, 0,
    KEY_BACKSPACE, 0, 0, KEY_END, 0, KEY_LEFT, KEY_HOME, 0, 0, 0, 0, 0,
    KEY_DOWN, 0, KEY_RIGHT, KEY_UP, KEY_ESC, 0, 0, 0, KEY_PAGEDOWN, 0,
    0, KEY_PAGEUP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

static unsigned int asusec_kp_sci_table[] =
  { 0, KEY_DELETE, KEY_F1, KEY_F2, KEY_F3, KEY_F4, KEY_F5,
    KEY_F6, KEY_F7, -9, -10, -11, -12, -13, -14, -15, KEY_F8,
    KEY_CONFIG, KEY_PREVIOUSSONG, KEY_PLAYPAUSE, KEY_NEXTSONG,
    KEY_MUTE, KEY_VOLUMEDOWN, KEY_VOLUMEUP };

#endif /* _ASUSEC_KEYPAD_H */
