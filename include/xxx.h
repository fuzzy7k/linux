#ifndef __XXX_H
#define __XXX_H

#define __DEBUG_XXX 1
#define XXX(fmt, ...)				\
  do { if( __DEBUG_XXX )			\
      printk(KERN_ERR				\
	     "XXX: (jack) [%s:%d]: "fmt"\n",	\
	     __func__, __LINE__,		\
	     ##__VA_ARGS__); } while(0)

#endif /* __XXX_H */
